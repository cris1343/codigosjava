package com.lapiscraft.lc.javabasico;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class NumPrimos {
	public static void a(int max, int cant) {
		List<Integer> lista = new ArrayList<>();
		Random aleat = new Random();
		for (int a = 0; a < cant; a++) {
			int b = aleat.nextInt(max) + 1;
			while (true) {
				if (!lista.contains(b)) {
					lista.add(b);
					break;
				} else {
					b = aleat.nextInt(max) + 1;
				}
			}
		}

		System.out.println(lista);

		for (int num : lista) {
			if (num == 2) {
				System.out.println("2 es primo y est� en el lugar " + String.valueOf(lista.indexOf(2) + 1));
				continue;
			}
			if (num == 1) {
				continue;
			}

			boolean primo = true;
			for (int b = (num - 1); b > 1; b--) {
				if (num % b == 0) {
					primo = false;
					break;
				}
			}

			if (primo)
				System.out.println(num + " es primo y est� en el lugar " + String.valueOf(lista.indexOf(num) + 1));
		}
	}

	public static int solicitMax(Scanner entrada) {
		try {
			System.out.println("Ingrese el n�mero natural m�ximo para revisar");
			int max = Integer.parseInt(entrada.next());
			if (max <= 0) {
				System.out.println("Lo ingresado no es un n�mero natural");
				System.out.println();

				return solicitMax(entrada);
			}
			return max;
		} catch (NumberFormatException e) {
			System.out.println("Lo ingresado no es un n�mero natural");
			System.out.println();

			return solicitMax(entrada);
		}
	}

	public static int solicitCant(Scanner entrada, int max) {
		try {
			System.out.println("Ingrese la cantidad de n�meros a revisar");
			int cant = Integer.parseInt(entrada.next());
			if (cant > max || cant <= 0) {
				System.out.println("El n�mero ingresado es mayor al m�ximo o no es natural");
				System.out.println();

				return solicitCant(entrada, max);
			}
			return cant;
		} catch (NumberFormatException e) {
			System.out.println("Lo ingresado no es un n�mero natural");
			System.out.println();

			return solicitCant(entrada, max);
		}
	}
}
