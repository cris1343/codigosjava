package com.lapiscraft.lc.javabasico;

import java.util.Scanner;

public class JavaBasico {
	public static void main(String[] args) {
		// System.out.println(SumarUnoACien.a());

		Scanner entrada = new Scanner(System.in);
		int max = NumPrimos.solicitMax(entrada);
		NumPrimos.a(max, NumPrimos.solicitCant(entrada, max));

		System.out.println("Programa finalizado");
	}
}
